from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:hithere@35.226.170.193/spotipy-me')
db = SQLAlchemy(app)

'''
The Artists table contains their name, id, primary genre, an img if available, a popularity metric, and their number of followers.
The Artists table is indexed by the unique artist id. The Artists table is also a foreign key to Albums and Tracks.
'''
class Artists(db.Model):
	__tablename__ = 'Artists'
	artist_name = db.Column(db.String(80), nullable = False)
	artist_id = db.Column(db.String(100), nullable = False, primary_key = True)
	genre = db.Column(db.String(80), nullable = False)
	img = db.Column(db.String(200), nullable = False)
	popularity = db.Column(db.Integer, nullable = False)
	followers = db.Column(db.Integer, nullable = False)

'''
The Albums table contains the album name, its id, the primary artist name, their id, an album cover image if available, the type of album, its release date, and the number of tracks it contains.
The Albums table is indexed by the unique album id. The Albums table contains a foreign key from Artists..
This provides the ability to search albums of a particular artist.
'''
class Albums(db.Model):
	__tablename__ = 'Albums'
	album_name = db.Column(db.String(80), nullable = False)
	album_id = db.Column(db.String(100), nullable = False, primary_key = True)
	artist_name = db.Column(db.String(80), nullable = False)
	artist_id = db.Column(db.String(100), nullable = False)
	img = db.Column(db.String(200), nullable = False)
	type = db.Column(db.String(50), nullable = False)
	release_date = db.Column(db.String(20), nullable = False)
	number_tracks = db.Column(db.Integer, nullable = False)

'''
The Tracks table contains the track name, its id, the artist's name, their id, the associated album id, the track type, its duration, and whether the track is explicit or not.
The Tracks table is indexed by the unique track id. The Tracks table contains foreign keys from Albums and Artists.
This provides the ability to search Tracks of a particular album or artist.
'''
class Tracks(db.Model):
	__tablename__ = 'Tracks'
	track_name = db.Column(db.String(80), nullable = False)
	track_id = db.Column(db.String(80), nullable = False, primary_key = True)
	artist_name = db.Column(db.String(80), nullable = False)
	artist_id = db.Column(db.String(80), nullable = False)
	album_id = db.Column(db.String(80), nullable = False)
	type = db.Column(db.String(80), nullable = False)
	duration = db.Column(db.Integer, nullable = False)
	explicit = db.Column(db.String(20), nullable = False)
	img = db.Column(db.String(200), nullable = False)

db.create_all()
