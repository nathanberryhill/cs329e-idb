import os
import sys
import unittest
from models import db, Albums, Artists, Tracks

class DBTestCases(unittest.TestCase):
	def test_add_artist(self):
		s = Artists(artist_name="Nathan",artist_id="11",genre="pop",img="href",popularity="5",followers="5")
		db.session.add(s)
		db.session.commit()

		r = db.session.query(Artists).filter_by(artist_id='11').first()
		self.assertEqual(str(r.artist_name),'Nathan')
		self.assertEqual(r.artist_id,'11')
		self.assertEqual(r.genre,'pop')
		self.assertEqual(r.img,'href')
		self.assertEqual(r.popularity,5)
		self.assertEqual(r.followers,5)

		db.session.delete(s)
		db.session.commit()

	def test_add_album(self):
		s = Albums(album_name="Forego",album_id="15",artist_name="The Quad Squad",artist_id="1",img="href",type="single",release_date="today",number_tracks="1")
		db.session.add(s)
		db.session.commit()

		r = db.session.query(Albums).filter_by(album_id='15').first()
		self.assertEqual(r.album_name,'Forego')
		self.assertEqual(r.album_id,'15')
		self.assertEqual(r.artist_name,'The Quad Squad')
		self.assertEqual(r.artist_id,'1')
		self.assertEqual(r.img,'href')
		self.assertEqual(r.type,'single')
		self.assertEqual(r.release_date,'today')
		self.assertEqual(r.number_tracks,1)

		db.session.delete(s)
		db.session.commit()

	def test_add_track(self):
		s = Tracks(track_name="Why did we procrastinate?",track_id="001",artist_name="me",artist_id="002",album_id="my life",type="track",duration="10",explicit="False",img='href')
		db.session.add(s)
		db.session.commit()

		r = db.session.query(Tracks).filter_by(track_id='001').first()
		self.assertEqual(r.track_name,'Why did we procrastinate?')
		self.assertEqual(r.track_id,'001')
		self.assertEqual(r.artist_name,'me')
		self.assertEqual(r.artist_id,'002')
		self.assertEqual(r.album_id,'my life')
		self.assertEqual(r.type,'track')
		self.assertEqual(r.duration,10)
		self.assertEqual(r.explicit,'False')
		self.assertEqual(r.img,'href')

		db.session.delete(s)
		db.session.commit()

if __name__ == '__main__':
	unittest.main()
