from flask import Flask, render_template, request
from models import db, Artists, Albums, Tracks
from sqlalchemy import cast, Integer, desc, asc
import subprocess


app = Flask(__name__)

artists_s = db.session.query(Artists)
albums_s = db.session.query(Albums)
tracks_s = db.session.query(Tracks)

artists_db = artists_s.paginate(1,6,False)
albums_db = albums_s.paginate(1,6,False)
tracks_db = tracks_s.paginate(1,6,False)

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/test/')
def test():
	p = subprocess.Popen(["coverage", "run", "--branch", "tests.py"],
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			stdin=subprocess.PIPE)
	out, err = p.communicate()
	output = err + out
	output = output.decode("utf-8") #convert from byte type to string type
	return render_template('test.html', output = "<br/>".join(output.split("\n")))

@app.route('/albums/')
def albums():
	return render_template('albums.html', albums=albums_db)

@app.route('/albums/<int:page>',methods=['GET'])
def albumsPage(page=1):
        sort = str(request.args.get('sort'))
        direction = str(request.args.get('direction'))
        search_term = request.args.get('search')
        if search_term is not None and search_term != '':
                albums_db = albums_s.filter(Albums.album_name.contains(str(search_term))).paginate(page,6,False)
        else:
                if sort == 'number_tracks':
                        sort = cast(Albums.number_tracks, Integer)
                if direction == 'asc':
                        albums_db = albums_s.order_by(asc(sort)).paginate(page,6,False)
                elif direction == 'desc':
                        albums_db = albums_s.order_by(desc(sort)).paginate(page,6,False)
                else:
                        albums_db = albums_s.paginate(page,6,False)
        return render_template('albums.html', albums=albums_db)

@app.route('/artists/')
def artists():
	return render_template('artists.html', artists=artists_db)

@app.route('/artists/<int:page>',methods=['GET'])
def artistsPage(page=1):
        sort = str(request.args.get('sort'))
        direction = str(request.args.get('direction'))
        search_term = request.args.get('search')
        if search_term is not None and search_term != '':
                artists_db = artists_s.filter(Artists.artist_name.contains(str(search_term))).paginate(page,6,False)
        else:
                if sort == 'popularity':
                        sort = cast(Artists.popularity, Integer)
                elif sort == 'followers':
                        sort = cast(Artists.followers, Integer)
                if direction == 'asc':
                        artists_db = artists_s.order_by(asc(sort)).paginate(page,6,False)
                elif direction == 'desc':
                        artists_db = artists_s.order_by(desc(sort)).paginate(page,6,False)
                else:
                        artists_db = artists_s.paginate(page,6,False)
        return render_template('artists.html', artists=artists_db)

@app.route('/tracks/')
def tracks():
	return render_template('tracks.html', tracks=tracks_db)

@app.route('/tracks/<int:page>',methods=['GET'])
def tracksPage(page=1):
        sort = str(request.args.get('sort'))
        direction = str(request.args.get('direction'))
        search_term = request.args.get('search')
        if search_term is not None and search_term != '':
                tracks_db = tracks_s.filter(Tracks.track_name.contains(str(search_term))).paginate(page,6,False)
        else:
                if sort == 'duration':
                        sort = cast(Tracks.duration, Integer)
                if direction == 'asc':
                        tracks_db = tracks_s.order_by(asc(sort)).paginate(page,6,False)
                elif direction == 'desc':
                        tracks_db = tracks_s.order_by(desc(sort)).paginate(page,6,False)
                else:
                        tracks_db = tracks_s.paginate(page,6,False)
        return render_template('tracks.html', tracks=tracks_db)

@app.route('/about/')
def about():
	return render_template('about.html')

## --- paths!
@app.route('/artists/<path:path>')
def artistsPath(path):
	artist_tracks = tracks_s.filter_by(artist_id=str(path)).limit(5).all()
	artist_albums = albums_s.filter_by(artist_id=str(path)).limit(5).all()
	artistInfo = artists_s.filter_by(artist_id=str(path)).first()
	return render_template('artists/path.html', artistInfo=artistInfo, artist_tracks=artist_tracks,artist_albums=artist_albums)

@app.route('/albums/<path:path>')
def albumsPath(path):
	albumInfo = albums_s.filter_by(album_id=str(path)).first()
	album_tracks = tracks_s.filter_by(album_id=str(path)).all()
	return render_template('albums/path.html', albumInfo=albumInfo, album_tracks=album_tracks)

@app.route('/tracks/<path:path>')
def tracksPath(path):
	trackInfo = tracks_s.filter_by(track_id=str(path)).first()
	albumInfo = albums_s.filter_by(album_id=str(trackInfo.album_id)).first()
	artistInfo = artists_s.filter_by(artist_id=str(trackInfo.artist_id)).first()
	return render_template('tracks/path.html', trackInfo=trackInfo, artistInfo=artistInfo, albumInfo=albumInfo)


if __name__ == "__main__":
	app.run(host='128.83.120.251')
